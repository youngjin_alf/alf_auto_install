@ECHO off
cls
set mypath=%cd%
set DataDir = 'C:\Program Files\Microsoft SQL Server\MSSQL15.ALFINSTANCE\MSSQL\data'

ECHO ************************* Starting MS SQL express installation *********************
CALL %mypath%\source\CreateSQL.bat

ECHO.
ECHO ************************* Restoring DB *****************************
CALL %mypath%\source\CreateDB.bat

ECHO.
ECHO ************************* Installing ALF Billing monitor *********************
CALL %mypath%\source\alfbill.bat

ECHO.
ECHO ************************* ALF Auto Installation COMPLETE *********************
pause


