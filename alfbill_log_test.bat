@ECHO off

set mypath=%cd%
SETLOCAL
set LOGFILE_DATE=%DATE:~0,4%-%DATE:~5,2%-%DATE:~8,2%
set LOGFILE_TIME=%TIME:~0,2%%TIME:~3,2%
set LOGFILE=ALF_Billing_%LOGFILE_DATE%_%LOGFILE_TIME%.log

CALL %mypath%\source\alfbill.bat > %mypath%\log\%LOGFILE%
pause