REM mypath=%cd% this is from parent path
:start
set DataDir=%C:\Program Files\Microsoft SQL Server\MSSQL15.ALFINSTANCE\MSSQL\data%
set mypath=%cd%

REM Asking if the use wants to rename ALF DBs
ECHO.
ECHO How do you want to call ALF database in SQL server?
ECHO Y - Default: ALF
Echo N - Rename the database
set choice=
set /p choice=Type Y/N to proceed:
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='Y' goto Default
if '%choice%'=='N' goto Rename

ECHO.
goto start
:Default
ren %mypath%\source\*.bak alf.bak
set rename=alf
ECHO ***************************************************************************
ECHO ALF Database name is set to default COMPLETE
ECHO ***************************************************************************
goto end

:Rename
echo.
ECHO Rename the database
set rename=
set /p rename=Type the name of the database:
ren %mypath%\source\*.bak %rename%.bak
set rename=%rename%
ECHO ***************************************************************************
ECHO Renaming ALF Database COMPLETE
ECHO ***************************************************************************
goto end

:end
REM installing the ALF DB
CALL sqlcmd.exe -S (local)\ALFINSTANCE -d master -b -U sa -P ALF@dmin99 -v mypath="%mypath%\source" -i %mypath%\source\CreateALFDB.sql 

ECHO ***************************************************************************
ECHO Restoring ALF Database COMPLETE
ECHO ***************************************************************************