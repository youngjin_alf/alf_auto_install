@ECHO off
REM this is to test retrieving filename from alf_%client%.bak individually
for %%F in ("%cd%\*.bak") do (
    set file=%%~nF
)
IF not exist %cd%\*.bak (ECHO ALF backup file does not exist. Abort.) 
echo done copying files: %file%
pause