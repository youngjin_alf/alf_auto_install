--Update data string
--declare 

USE [master];
GO
RESTORE DATABASE $(rename)
FROM DISK = '$(mypath)\$(rename).bak'
WITH MOVE 'alf_peterson' TO '$(DataDir)\$(rename).mdf',
MOVE 'alf_peterson_log' TO '$(DataDir)\$(rename).ldf'

CREATE LOGIN alfadmin WITH PASSWORD = 'alfadmin'
GO

GO

Use $(rename);
GO

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'alfadmin')
BEGIN
    CREATE USER alfadmin FOR LOGIN alfadmin
    EXEC sp_addrolemember N'db_owner', N'alfadmin'
END;
GO