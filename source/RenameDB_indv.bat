@ECHO off

:start
set mypath=%cd%

ECHO.
ECHO How do you want to call ALF database in SQL server?
ECHO 1. Default: ALF
Echo 2. Rename the database
set choice=
set /p choice=Type the number to proceed:
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='1' goto Default
if '%choice%'=='2' goto Rename

ECHO.
goto start
:Default
ren %mypath%\source\*.bak alf.bak
ECHO ***************************************************************************
ECHO ALF Database name is set to default COMPLETE
ECHO ***************************************************************************
goto end

:Rename
echo.
ECHO Rename the database
set rename=
set /p rename=Type the name of the database:
ren %mypath%\source\*.bak %rename%.bak
ECHO ***************************************************************************
ECHO Renaming ALF Database COMPLETE
ECHO ***************************************************************************
goto end
:end

pause