How to zip and provide it to clients
- copy alf_%client%.bak and paste into 'source' folder
- zip 'source' folder, readme.txt and ALF_AUTO_INSTALLER.bat
- Then, provide the zip file to user

CONTENTS
readme.txt
ALF_AUTO_INSTALLER.bat
source
- ALF Billing monitor v1.0.24.0
- ConfigurationFile.ini
- ALF DB
- alfbill.bat
- CreateALFDB.sql
- CreateDB.bat
- CreateSQL.bat
- startALFBill.bat