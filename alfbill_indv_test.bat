@ECHO off
cls
:start
set mypath=%cd%
REM Retrieving ALF DB file name
for %%F in ("%cd%\source\*.bak") do (
    set filename=%%~nF
)
IF not exist %cd%\source\*.bak (ECHO ALF backup file does not exist. Abort. Please run the ALF Billing monitor exe file manually. 
pause
goto end) 

ECHO.
ECHO ************************* Installing ALF Billing monitor *********************

ECHO.
ECHO Do you want to include installing ALF billing monitor?
ECHO Y. Yes
Echo N. No
set choice=
set /p choice=Type the number to proceed.
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='Y' goto Yes
if '%choice%'=='N' goto No

ECHO.
goto start
:Yes
ECHO **** Include ALF billing monitor installation within auto ALF installation process ****
if not exist "C:\Program Files (x86)\ALF" mkdir "C:\Program Files (x86)\ALF"
CALL %mypath%\source\alfbill-win32-install_1.0.24.0.exe /s /v" INSTALLDIR=C:\Program Files (x86)\ALF" dbtype=SQLServer dbName=(local)\ALFINSTANCE@%filename% dbUser=alfadmin dbPasswd=alfadmin
Powershell -Command "& { Start-Process \"%mypath%\source\startALFBill.bat\" -verb RunAs}"
pause

goto end
:No
ECHO **** Exclude ALF billing monitor within auto ALF installation process ****
pause