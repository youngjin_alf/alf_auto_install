ALF Windows version auto installation with SQL server express
v1.0

HOW TO USE
Download MS SQL express
- Download any version of MS SQL express equal to or later than v2012
- run the exe file ex) SQL2019-SSEI-Expr.exe
- Once the GUI opens, Click 'Download Media'
- Choose 'Express Core' and select download location then click 'Download'
- Run SQLEXPR_x64_ENU.exe to unzip the folder (Click ok to proceed)
- Once the progress bar disappears files will be unzipped you will see the SQL Server Installation window 
- Close the window and you should see the SQLEXPR_x64_ENU folder created
- Copy the folder and go to 'ALF_AUTO_INSTALLER.ZIP' instruction on the bottom

ALF_AUTO_INSTALLER.zip
- unzip the folder anywhere in a server
- Paste the copied folder (SQLEXPR_x64_ENU) and put it into the 'source' folder (so the folder path should be ".\alf_auto_install\source\SQLEXPR_x64_ENU")
- copy ConfigurationFile.ini from 'source' folder and paste it into the copied folder (SQLEXPR_x64_ENU)

It's now ready to start!
- Run ALF_AUTO_INSTALLER.bat to proceed
- After installing the server, it will ask whether you want to rename the database. Then, it will restore the database.
- After restoring the database, it will ask whether you want to install the billing monitor (if you don't want to include within the process somehow, it can be found in ./source/alfbill-win32-install_1.0.24.0.exe)


CONTENTS
readme.txt
ALF_AUTO_INSTALLER.bat
source
- ALF Billing monitor v1.0.24.0
- ALF DB
- ConfigurationFile.ini
- alfbill.bat
- CreateALFDB.sql
- CreateDB.bat
- CreateSQL.bat
- startALFBill.bat
